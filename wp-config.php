<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ksoft' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'BV7&M|9-E9+gl@N9Ga6+5GZ%E~U:6;3ajoT<#&`^-ZiHl$DpoH&o#a}LneH8&|5_' );
define( 'SECURE_AUTH_KEY',  'J=O<}n:13G7el{c]>Kxag:Cm[J;_YH{(wT(M`k>,=3z2)$YB8+K=e>z,CSsOz,4c' );
define( 'LOGGED_IN_KEY',    'Sy|^.h*O/XJQ>Ng@~}RPeVoVmM,r* Bhn& 7urUya%|`~Wc~RWWC>xRkwkq9RIv0' );
define( 'NONCE_KEY',        ':&jiYR{Vz@a$j(ea(Pu$MBTDH3E05xfQ,b:wqsPcOH:Fm^(cH M/+-{Iv_dt`^i0' );
define( 'AUTH_SALT',        '/dJ6uz{tn8k/D5.^zixajDv9;GI)UtNqiWzmulQ7f!/2G-p$JgT^_aqie[S;M+Ie' );
define( 'SECURE_AUTH_SALT', '{<WM&<fwr5/_<mVG[#_m{+#CLYe,kn09EnnO8(kFqXe*){z>5e7%u_rE8Zf3kV]%' );
define( 'LOGGED_IN_SALT',   'yda]YyXX&%Dt`ZM(5>j( -(=PQ3q=rKkz8vE!g#FGN!tNbOTFqJO#H)blxDrzXEZ' );
define( 'NONCE_SALT',       'SLj!`D_WSuvo~cZ7sW~<,Ba=Ma?yT[Rn>l^F.HF?Eg$(Hb|Q>}k,s7g$j$_t(b (' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ks_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
